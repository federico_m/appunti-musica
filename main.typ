#import "template.typ": *
#import "@preview/conchord:0.2.0": new-chordgen


// Take a look at the file `template.typ` in the file panel
// to customize this template and discover how it works.
#show: project.with(
  title: "Appunti musica",
  revision: "1"
)

#let chord = new-chordgen(
  use-shadow-barre:false, 
  scale-length:0.87pt,
  colors: (
    grid: gray.darken(30%),
    hold: blue,
    barre: navy
  )
)

= Armonizzazione scala maggiore

== Sviluppo armonico

#align(center)[
  #table(
    columns: 8,
    [], [I], [II], [III], [IV], [V], [VI], [VII],
    
    [#chord-normal("C")], [#chord-major-seventh("C")], [#chord-minor-seventh("D")], [#chord-minor-seventh("E")], [#chord-major-seventh("F")], [#chord-dominant-seventh("G")], [#chord-minor-seventh("A")], [#chord-half-diminished("B")],
    
    [#chord-normal("D")], [#chord-major-seventh("D")], [#chord-minor-seventh("E")], [#chord-minor-seventh("F", sharp: music.sharp)], [#chord-major-seventh("G")], [#chord-dominant-seventh("A")], [#chord-minor-seventh("B")], [#chord-half-diminished("C", sharp: music.sharp)],
    
    [#chord-normal("E", sharp: music.flat)], [#chord-major-seventh("E", sharp: music.flat)], [#chord-minor-seventh("F")], [#chord-minor-seventh("G")], [#chord-major-seventh("A", sharp: music.flat)], [#chord-dominant-seventh("B", sharp: music.flat)], [#chord-minor-seventh("C")], [#chord-half-diminished("D")],
    
    [#chord-normal("E")], [#chord-major-seventh("E")], [#chord-minor-seventh("F", sharp: music.sharp)], [#chord-minor-seventh("G", sharp: music.sharp)], [#chord-major-seventh("A")], [#chord-dominant-seventh("B")], [#chord-minor-seventh("C", sharp: music.sharp)], [#chord-half-diminished("D", sharp: music.sharp)],
    
    [#chord-normal("F")], [#chord-major-seventh("F")], [#chord-minor-seventh("G")], [#chord-minor-seventh("A")], [#chord-major-seventh("B", sharp: music.flat)], [#chord-dominant-seventh("C")], [#chord-minor-seventh("E")], [#chord-half-diminished("E")],
    
    [#chord-normal("G")], [#chord-major-seventh("G")], [#chord-minor-seventh("A")], [#chord-minor-seventh("B")], [#chord-major-seventh("C")], [#chord-dominant-seventh("D")], [#chord-minor-seventh("E")], [#chord-half-diminished("F", sharp: music.sharp)],
    
    [#chord-normal("A", sharp: music.flat)], [#chord-major-seventh("A", sharp: music.flat)], [#chord-minor-seventh("B", sharp: music.flat)], [#chord-minor-seventh("C")], [#chord-major-seventh("D", sharp: music.flat)], [#chord-dominant-seventh("E", sharp: music.flat)], [#chord-minor-seventh("F")], [#chord-half-diminished("G")],
    
    [#chord-normal("A")], [#chord-major-seventh("A")], [#chord-minor-seventh("B")], [#chord-minor-seventh("C", sharp: music.sharp)], [#chord-major-seventh("D")], [#chord-dominant-seventh("E")], [#chord-minor-seventh("F", sharp: music.sharp)], [#chord-half-diminished("G", sharp: music.sharp)],
    
    [#chord-normal("B", sharp: music.flat)], [#chord-major-seventh("B", sharp: music.flat)], [#chord-minor-seventh("C")], [#chord-minor-seventh("D")], [#chord-major-seventh("E", sharp: music.flat)], [#chord-dominant-seventh("F")], [#chord-minor-seventh("G")], [#chord-half-diminished("A")],

    [#chord-normal("B")], [#chord-major-seventh("B")], [#chord-minor-seventh("C", sharp: music.sharp)], [#chord-minor-seventh("D", sharp: music.sharp)], [#chord-major-seventh("E")], [#chord-dominant-seventh("F", sharp: music.sharp)], [#chord-minor-seventh("G", sharp: music.sharp)], [#chord-half-diminished("A", sharp: music.sharp)],
  )
]

== Voicing

=== Basso 6° corda

==== Voicing 1-3-5-7
#box(chord("1,x,x,2,1,0,*", name:chord-major-seventh("F")))
#box(chord("3,x,x,4,3,1,*", name:chord-dominant-seventh("G")))
#box(chord("5,x,x,5,5,3,*", name:chord-minor-seventh("A")))
#box(chord("7,x,x,7,6,5", name:chord-half-diminished("B")))
#box(chord("8,x,x,9,8,7,*", name: chord-major-seventh("C")))
#box(chord("10,x,x,10,10,8,*", name:chord-minor-seventh("D")))
#box(chord("12,x,x,12,12,10,*", name:chord-minor-seventh("E")))

==== Voicing 1-5-7-3
#box(chord("1,3,2,2,x,x,*", name:chord-major-seventh("F")))
#box(chord("3,5,3,4,x,x,*", name:chord-dominant-seventh("G")))
#box(chord("5,7,5,5,x,x,*", name:chord-minor-seventh("A")))
#box(chord("7,8,7,7,x,x,*", name:chord-half-diminished("B")))
#box(chord("8,10,9,9,x,x,*", name:chord-major-seventh("C")))
#box(chord("10,12,10,10,x,x,*", name:chord-minor-seventh("D")))
#box(chord("12,14,12,12,x,x,*", name:chord-minor-seventh("E")))

==== Voicing 1-7-3-5
#box(chord("1,x,2,2,1,x,*", name:chord-major-seventh("F")))
#box(chord("3,x,3,4,3,x,*", name:chord-dominant-seventh("G")))
#box(chord("5,x,5,5,5,x,*", name:chord-minor-seventh("A")))
#box(chord("7,x,7,7,6,x", name:chord-half-diminished("B")))
#box(chord("8,x,9,9,8,x,*", name:chord-major-seventh("C")))
#box(chord("10,x,10,10,10,x,*", name:chord-minor-seventh("D")))
#box(chord("12,x,12,12,12,x,*", name:chord-minor-seventh("E")))

==== Voicing 3-1-5-7
#box(chord("3,x,2,4,3,x", name:chord-minor-seventh("E")))
#box(chord("5,x,3,5,5,x,*", name:chord-major-seventh("F")))
#box(chord("7,x,5,7,6,x", name:chord-dominant-seventh("G")))
#box(chord("8,x,7,9,8,x", name:chord-minor-seventh("A")))
#box(chord("10,x,9,10,10,x", name:chord-half-diminished("B")))
#box(chord("12,x,10,12,12,x", name:chord-major-seventh("C")))
#box(chord("13,x,12,14,13,x", name:chord-minor-seventh("D")))

==== Voicing 3-7-1-5
#box(chord("3,5,2,4,x,x", name:chord-minor-seventh("E")))
#box(chord("5,7,3,5,x,x,*", name:chord-major-seventh("F")))
#box(chord("7,8,5,7,x,x", name:chord-dominant-seventh("G")))
#box(chord("8,10,7,9,x,x", name:chord-minor-seventh("A")))
#box(chord("10,10,9,10,x,x", name:chord-half-diminished("B")))
#box(chord("12,14,10,12,x,x", name:chord-major-seventh("C")))
#box(chord("13,15,12,14,x,x", name:chord-minor-seventh("D")))

==== Voicing 5-1-3-7
#box(chord("3,3,2,4,x,x", name:chord-major-seventh("C")))
#box(chord("5,5,3,5,x,x", name:chord-minor-seventh("D")))
#box(chord("7,7,5,7,x,x", name:chord-minor-seventh("E")))
#box(chord("8,8,7,9,x,x,*", name:chord-major-seventh("F")))
#box(chord("10,10,9,10,x,x", name:chord-dominant-seventh("G")))
#box(chord("12,12,10,12,x,x", name:chord-minor-seventh("A")))
#box(chord("12,13,11,13,x,x", name:chord-half-diminished("B")))

=== Basso 5° corda

==== Voicing 1-3-5-7
#box(chord("x,3,2,0,0,x,*", name:chord-major-seventh("C")))
#box(chord("x,5,3,2,1,x,*", name:chord-minor-seventh("D")))
#box(chord("x,7,5,4,3,x,*", name:chord-minor-seventh("E")))
#box(chord("x,8,7,5,5,x,*", name:chord-major-seventh("F")))
#box(chord("x,10,9,7,6,x,*", name:chord-dominant-seventh("G")))
#box(chord("x,12,10,9,8,x,*", name:chord-minor-seventh("A")))
#box(chord("x,14,12,10,10,x,*", name:chord-half-diminished("B")))

==== Voicing 1-5-7-3
#box(chord("x,2,3,2,3,x,*", name:chord-half-diminished("B")))
#box(chord("x,3,5,4,5,x,*", name:chord-major-seventh("C")))
#box(chord("x,5,7,5,6,x,*", name:chord-minor-seventh("D")))
#box(chord("x,7,9,7,8,x,*", name:chord-minor-seventh("E")))
#box(chord("x,8,10,9,10,x,*", name:chord-major-seventh("F")))
#box(chord("x,10,12,10,12,x,*", name:chord-dominant-seventh("G")))
#box(chord("x,12,14,12,13,x,*", name:chord-minor-seventh("A")))

==== Voicing 1-7-3-5
#box(chord("x,2,x,2,3,1,*", name:chord-half-diminished("B")))
#box(chord("x,3,x,4,5,3,*", name:chord-major-seventh("C")))
#box(chord("x,5,x,5,6,5,*", name:chord-minor-seventh("D")))
#box(chord("x,7,x,7,8,7,*", name:chord-minor-seventh("E")))
#box(chord("x,8,x,9,10,8,*", name:chord-major-seventh("F")))
#box(chord("x,10,x,10,12,10,*", name:chord-dominant-seventh("G")))
#box(chord("x,12,x,12,13,12,*", name:chord-minor-seventh("A")))

==== Voicing 3-7-1-5
#box(chord("x,3,5,2,5,x,*", name:chord-minor-seventh("A")))
#box(chord("x,5,7,4,6,x,*", name:chord-half-diminished("B")))
#box(chord("x,7,9,5,8,x,*", name:chord-major-seventh("C")))
#box(chord("x,8,10,7,10,x,*", name:chord-minor-seventh("D")))
#box(chord("x,10,12,9,12,x,*", name:chord-minor-seventh("E")))
#box(chord("x,12,14,10,13,x,*", name:chord-major-seventh("F")))
#box(chord("x,14,15,12,15,x,*", name:chord-dominant-seventh("G")))

==== Voicing 5-1-7-3
#box(chord("x,2,2,0,3,x,*", name:chord-minor-seventh("E")))
#box(chord("x,3,3,2,5,x,*", name:chord-major-seventh("F")))
#box(chord("x,5,5,4,6,x,*", name:chord-dominant-seventh("G")))
#box(chord("x,7,7,5,8,x,*", name:chord-minor-seventh("A")))
#box(chord("x,8,9,7,10,x,*", name:chord-half-diminished("B")))
#box(chord("x,10,10,9,12,x,*", name:chord-major-seventh("C")))
#box(chord("x,12,12,10,13,x,*", name:chord-minor-seventh("D")))

=== Basso 4° corda

==== Voicing 1-3-5-7
#box(chord("x,x,3,2,1,0,*", name:chord-major-seventh("F")))
#box(chord("x,x,5,4,3,1,*", name:chord-dominant-seventh("G")))
#box(chord("x,x,7,5,5,3,*", name:chord-minor-seventh("A")))
#box(chord("x,x,9,7,6,5,*", name:chord-half-diminished("B")))
#box(chord("x,x,10,9,8,7,*", name:chord-major-seventh("C")))
#box(chord("x,x,12,10,10,8,*", name:chord-minor-seventh("D")))
#box(chord("x,x,14,12,12,10,*", name:chord-minor-seventh("E")))

==== Voicing 1-5-7-3
#box(chord("x,x,2,4,3,3,*", name:chord-minor-seventh("E")))
#box(chord("x,x,3,5,5,5,*", name:chord-major-seventh("F")))
#box(chord("x,x,5,7,6,7,*", name:chord-dominant-seventh("G")))
#box(chord("x,x,7,9,8,8,*", name:chord-minor-seventh("A")))
#box(chord("x,x,9,10,10,10,*", name:chord-half-diminished("B")))
#box(chord("x,x,11,13,13,13,*", name:chord-major-seventh("C")))
#box(chord("x,x,12,14,13,13,*", name:chord-minor-seventh("D")))

==== Voicing 3-7-1-5
#box(chord("x,x,2,4,1,3,*", name:chord-major-seventh("C")))
#box(chord("x,x,3,5,3,5,*", name:chord-minor-seventh("D")))
#box(chord("x,x,5,7,5,7,*", name:chord-minor-seventh("E")))
#box(chord("x,x,7,9,6,8,*", name:chord-major-seventh("F")))
#box(chord("x,x,9,10,8,10", name:chord-dominant-seventh("G")))
#box(chord("x,x,9,11,9,11,*", name:chord-minor-seventh("A")))
#box(chord("x,x,11,13,11,12,*", name:chord-half-diminished("B")))

==== Voicing 5-1-3-7
#box(chord("x,x,2,2,1,3,*", name:chord-minor-seventh("A")))
#box(chord("x,x,3,4,3,5,*", name:chord-half-diminished("B")))
#box(chord("x,x,5,5,5,7,*", name:chord-major-seventh("C")))
#box(chord("x,x,7,7,6,8,*", name:chord-minor-seventh("D")))
#box(chord("x,x,9,9,8,10,*", name:chord-minor-seventh("E")))
#box(chord("x,x,10,10,10,12,*", name:chord-major-seventh("F")))
#box(chord("x,x,12,12,12,13,*", name:chord-dominant-seventh("G")))

==== Voicing 5-7-3-5
#box(chord("x,x,3,2,3,1,*", name:chord-half-diminished("B")))
#box(chord("x,x,5,4,5,3,*", name:chord-major-seventh("C")))
#box(chord("x,x,7,5,6,5,*", name:chord-minor-seventh("D")))
#box(chord("x,x,9,7,8,7,*", name:chord-minor-seventh("E")))
#box(chord("x,x,10,9,10,8,*", name:chord-major-seventh("F")))
#box(chord("x,x,12,10,12,10,*", name:chord-dominant-seventh("G")))
#box(chord("x,x,14,12,13,12,*", name:chord-minor-seventh("A")))

==== Voicing 7-3-5-1
#box(chord("x,x,2,2,1,1,*", name:chord-major-seventh("F")))
#box(chord("x,x,3,4,3,3,*", name:chord-dominant-seventh("G")))
#box(chord("x,x,5,5,5,5,*", name:chord-minor-seventh("A")))
#box(chord("x,x,7,7,6,7,*", name:chord-half-diminished("B")))
#box(chord("x,x,9,9,8,8,*", name:chord-major-seventh("C")))
#box(chord("x,x,10,10,10,10,*", name:chord-minor-seventh("D")))
#box(chord("x,x,12,12,12,12,*", name:chord-minor-seventh("E")))

== Progressioni

=== I iii IV V (I)
#box(chord("8,x,x,9,8,8,*", name:chord-normal("C")))
#box(chord("x,7,x,7,8,7,*", name:chord-minor-seventh("E")))
#box(chord("x,8,x,5,8,5,*", name:chord-ninth("F")))
#box(chord("3,x,x,4,6,5,*", name:chord-ninth("G")))
#box(chord("x,3,x,4,5,3,*", name:chord-major-seventh("C")))

#box(chord("8,x,5,5,5,x,*", name:chord-normal("C")))
#box(chord("7,x,5,7,8,x,*", name:chord-minor-seventh("E", bass: "G")))
#box(chord("5,x,3,5,5,x,*", name:chord-major-seventh("F", bass: "A")))
#box(chord("3,x,3,5,3,x,*", name:chord-sus-fourth("G")))
#box(chord("3,x,3,4,3,x,*", name:chord-dominant-seventh("G")))
#box(chord("x,3,5,5,5,x,*", name:chord-normal("C")))

= Accordi interessanti

#box(chord("x,8,x,5,8,5,*", name:chord-ninth("F")))
#box(chord("x,8,7,5,8,x,*", name:chord-ninth("F")))
#box(chord("x,3,2,0,3,x,*", name:chord-add-ninth("C")))
#box(chord("x,5,4,0,3,x,*", name:chord-add-fourth("D")))
#box(chord("5,x,3,5,3,x,*", name:chord-minor-seventh("D", bass: "A")))