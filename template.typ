#let project(title: "", revision:"", body) = {
  // Set the document's basic properties.
  set document(
    title: title
  )

  set table(
    stroke: none,
    gutter: 0.2em,
    fill: (x, y) => if x == 0 or y == 0 { gray },
    inset: (right: 1.5em),
  )
  show table.cell: it => {
    if it.x == 0 or it.y == 0 {
      set text(white)
      strong(it)
    } else if it.body == [] {
      // Replace empty cells with 'N/A'
      pad(..it.inset)[_N/A_]
    } else {
      it
    }
  }
  
  set page(
    numbering: "1", 
    number-align: center,
    background: context {
     if counter(page).get().first() == 1 [
       #image(width: 100%,"background.jpg")
     ]
    },
    footer: context {
      if counter(page).get().first() > 1 [
        #h(1fr)
        #counter(page).display("1")
        #h(1fr)
      ]
    }
  )
  set text( lang: "it")
  
  v(3fr)
  
  align(center)[
    #rect(
      width: 60%,
      fill: white,
      radius: 10pt,
      inset: (
        top: 3em,
        bottom: 1.5em,
      ),
      stack(
        text(
          weight: 700, 
          1.75em,
          upper(title)
        ),
        v(3em),
        text(
          weight: "thin",
          0.8em,
          "Rev: " + revision
        )
      )
    )
  ]  
  v(7fr)
  
  pagebreak()
  
  set heading(numbering: (..n) => {
    if n.pos().len() < 3 {
      numbering("1.1", ..n)
    } 
  })
 
  show outline.entry.where(
    level: 1
  ): it => {
    v(12pt, weak: true)
    strong(it)
  }

  show outline.entry.where(
    level: 2
  ): it => {
    h(1em)
    it
  }
  
  show outline.entry.where(
    level: 3
  ): it => {
    h(2em)
    it
  }
  outline(
    depth: 3
  )
  pagebreak()
  
  // Main body.
  set par(justify: true)
 
  body
}

#let music = symbol(
  ("major-seventh", "∆"),
  ("diminished", "º"),
  ("half-diminished", "ø"),
  ("flat", str.from-unicode(9837)),
  ("sharp", str.from-unicode(9839)),
)

#let chord-normal(chord, sharp: "", bass: "") = chord + sharp + if( bass != "" ) { "/" + bass }
#let chord-major-seventh(chord, sharp: "", bass: "") = chord + sharp + music.major-seventh + if( bass != "" ) { "/" + bass }
#let chord-dominant-seventh(chord, sharp: "", bass: "") = chord + sharp + "7" + if( bass != "" ) { "/" + bass }
#let chord-add-fourth(chord, sharp: "", bass: "") = chord + sharp + "add4" + if( bass != "" ) { "/" + bass }
#let chord-sus-fourth(chord, sharp: "", bass: "") = chord + sharp + "sus4" + if( bass != "" ) { "/" + bass }
#let chord-minor-ninth(chord, sharp: "", bass: "") = chord + sharp + "m9" + if( bass != "" ) { "/" + bass }
#let chord-ninth(chord, sharp: "", bass: "") = chord + sharp + "9" + if( bass != "" ) { "/" + bass }
#let chord-add-ninth(chord, sharp: "", bass: "") = chord + sharp + "add9" + if( bass != "" ) { "/" + bass }
#let chord-sus-ninth(chord, sharp: "", bass: "") = chord + sharp + "sus9" + if( bass != "" ) { "/" + bass }
#let chord-minor-seventh(chord, sharp: "", bass: "") = chord + sharp + "-7" + if( bass != "" ) { "/" + bass }
#let chord-half-diminished(chord, sharp: "", bass: "") = chord + sharp + music.half-diminished + if( bass != "" ) { "/" + bass }
#let chord-diminished(chord, sharp: "", bass: "") = chord + sharp + music.diminished + if( bass != "" ) { "/" + bass }

